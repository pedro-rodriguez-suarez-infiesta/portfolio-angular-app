export const ENDPOINTS = {
    homeMainUrl: 'http://localhost:3000/HomeMain',
    rrssUrl: 'http://localhost:3000/Rrss',
    homeAboutUrl: 'http://localhost:3000/HomeAbout',
    profileImagesUrl: 'http://localhost:3000/ProfileImages',
    galleriesMainUrl: 'http://localhost:3000/GalleriesMainImages',
    galleryFirstUrl: 'http://localhost:3000/GalleryFirst',
    gallerySecondUrl: 'http://localhost:3000/GallerySecond',
    galleryThirdUrl: 'http://localhost:3000/GalleryThird',
    galleryFourthUrl: 'http://localhost:3000/GalleryFourth',
    galleryVideoUrl: 'http://localhost:3000/GalleryVideo',
    contactMainUrl: 'http://localhost:3000/ContactMain',
    postUserUrl: 'http://localhost:3000/RegisteredUser',


};
