import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home-components/home.component';
import { HomeMainComponent } from './home-components/home-main/home-main.component';
import { HomeAboutComponent } from './home-components/home-about/home-about.component';


@NgModule({
  declarations: [HomeComponent, HomeMainComponent, HomeAboutComponent],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
