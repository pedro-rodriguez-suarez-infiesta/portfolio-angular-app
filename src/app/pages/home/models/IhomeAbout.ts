export interface IhomeAbout {
    homeAboutTitle: string;
    homeAboutDescription: string;
    imageTitle: string;
    imageDescription: string;
    imageUrl: string;

}

/* export interface IprofileImages {
    imageTitle: string;
    imageDescription: string;
    imageUrl: string;
} */
