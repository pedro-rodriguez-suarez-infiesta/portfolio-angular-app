export interface IhomeMain {
    homeTitle: string;
    homeDescription: string;
    imageTitle: string;
    imageDescription: string;
    imageUrl: string;

}
