import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ENDPOINTS } from 'src/endpoints/endpoints';
import { IhomeAbout } from '../models/IhomeAbout';
import { IhomeMain } from '../models/IhomeMain';
import { IhomeRrss } from '../models/IhomeRrss';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  private homeEndpoint: string = ENDPOINTS.homeMainUrl;
  private homeAboutEndpoint: string = ENDPOINTS.homeAboutUrl;
  private rrssEndpoint: string = ENDPOINTS.rrssUrl;

  constructor(private http: HttpClient) {
    /*empty*/
  }
  public getHome(): Observable<IhomeMain[]> {
    return this.http.get(this.homeEndpoint).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('value expected');
        } else {
          return response;
        }
      }),
    );
  }
  public getHomeAbout(): Observable<IhomeAbout[]> {
    return this.http.get(this.homeAboutEndpoint).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('value expected');
        } else {
          return response;
        }
      }),
    );
  }
}
