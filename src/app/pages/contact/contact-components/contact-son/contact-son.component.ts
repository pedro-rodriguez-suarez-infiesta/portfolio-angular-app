import { Icontact, Form } from './../../models/Icontact';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContactService } from '../../contact-services/contact.service';



@Component({
  selector: 'app-contact-son',
  templateUrl: './contact-son.component.html',
  styleUrls: ['./contact-son.component.scss']
})
export class ContactSonComponent implements OnInit {
  public staticDataForm!: Icontact[];
  public reservationForm: FormGroup;
  public submitted = false;
  @Output() public newUserEmmit = new EventEmitter <Form>();

  constructor(
    private formBuilder: FormBuilder,
    private formService: ContactService,
  ) {
    this.reservationForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(1)]],
      surname: ['', [Validators.required, Validators.minLength(1)]],
      email: ['', [Validators.required, Validators.email]],
      message: ['', [Validators.required, Validators.maxLength(800)]],

    });
   }
   // tslint:disable-next-line: typedef
   public onSubmit() {
    this.submitted = true;
    if (this.reservationForm.valid) {
      const newVisitor: Form = {
        name: this.reservationForm?.get('name')?.value,
        surname: this.reservationForm?.get('surname')?.value,
        email: this.reservationForm?.get('email')?.value,
        message: this.reservationForm?.get('message')?.value,
      };
      this.formService.postUser(newVisitor).subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.error(err.message);
        }
      );
      this.newUserOutput(newVisitor);
      this.reservationForm.reset();
      this.submitted = false;
      console.log(newVisitor);
    }
  }
  // tslint:disable-next-line: typedef
  public newUserOutput(newVisitor: Form) {
    this.newUserEmmit.emit(newVisitor);
    }

  ngOnInit(): void {
    this.getFormStaticData();
  }

  public getFormStaticData(): void {
    this.formService.getFormStaticData().subscribe(
      (data: Icontact[]) => {
      console.log(data);
      this.staticDataForm = data;
    }
  );
  }

}
