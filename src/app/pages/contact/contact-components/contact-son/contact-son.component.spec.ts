import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactSonComponent } from './contact-son.component';

describe('ContactSonComponent', () => {
  let component: ContactSonComponent;
  let fixture: ComponentFixture<ContactSonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContactSonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactSonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
