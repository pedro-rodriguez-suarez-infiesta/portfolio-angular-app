import { Icontact } from './../models/Icontact';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  public form!: Icontact;

  constructor() { }

  ngOnInit(): void {
  }

  // tslint:disable-next-line: typedef
  public newVisitor(form: Icontact){
    this.form = form;
  }


}
