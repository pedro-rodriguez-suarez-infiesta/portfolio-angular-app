export interface Icontact {
    id: number;
    name: string;
    formDescription: string;
    image: string;

}



export interface Form {
    name: string;
    surname: string;
    email: string;
    message: string;
}

