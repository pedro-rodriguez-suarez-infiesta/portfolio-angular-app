import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home', pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then((m) => m.HomeModule)
  },

  {
    path: 'galleries',
    loadChildren: () => import('./pages/galleries/galleries.module').then((m) => m.GalleriesModule)
  },
  {
    path: 'contact',
    loadChildren: () => import ('./pages/contact/contact.module').then((m) => m.ContactModule)
  }

  /*  {
    path: 'contact',
    loadChildren: () => import ('./pages/form/form.module').then((m) => m.FormModule)
  } */
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
